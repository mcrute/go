package repository

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestPackageReturnsProperFilename(t *testing.T) {
	pkg := Package{
		Name:    "test-package",
		Version: "1.2.3-r0",
	}

	assert.Equal(t, "test-package-1.2.3-r0.apk", pkg.Filename())
}

func TestParsePackage(t *testing.T) {
	f, err := os.Open("testdata/hello-0.1.0-r0.apk")
	require.Nil(t, err)
	apk, err := ParsePackage(f)
	require.Nil(t, err)
	require.Equal(t, "hello", apk.Name)
	require.Equal(t, "0.1.0-r0", apk.Version)
	require.Equal(t, "x86_64", apk.Arch)
	require.Equal(t, "just a test package", apk.Description)
	require.Equal(t, "Apache-2.0", apk.License)
	require.Equal(t, []string{"busybox"}, apk.Dependencies)
	require.Equal(t, uint64(499), apk.Size)
	require.Equal(t, uint64(4117), apk.InstalledSize)
	require.Equal(t, "Q1DNWZeWkviN7MJedLpYM8yBvmnGM=", apk.ChecksumString())
	require.Equal(t, []byte{
		0xc, 0xd5, 0x99, 0x79, 0x69, 0x2f, 0x88, 0xde, 0xcc,
		0x25, 0xe7, 0x4b, 0xa5, 0x83, 0x3c, 0xc8, 0x1b, 0xe6,
		0x9c, 0x63}, apk.Checksum)
}
